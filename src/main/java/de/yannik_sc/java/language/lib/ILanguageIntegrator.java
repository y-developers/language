package de.yannik_sc.java.language.lib;

/**
 * @author Yannik_Sc
 */
public interface ILanguageIntegrator
{
    void integrate(ILanguageProvider provider);
}
