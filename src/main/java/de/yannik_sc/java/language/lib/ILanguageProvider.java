package de.yannik_sc.java.language.lib;

import de.yannik_sc.java.language.maps.NamespaceMap;

/**
 * @author Yannik_Sc
 */
public interface ILanguageProvider
{
    /**
     * @return the list of translations built like this: Namespace > Language identifier > Translation identifier > Translation
     */
    NamespaceMap getTranslations();
}
