package de.yannik_sc.java.language;

import de.yannik_sc.java.language.lib.ILanguage;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yannik_Sc
 */
public class Language implements ILanguage
{
    String language;
    Map<String, String> translations = new HashMap<>();

    public Language(String language)
    {
        this.language = language;
    }

    @Override
    public String getTranslation(String unlocalized)
    {
        if (this.translations.containsKey(unlocalized)) {
            return this.translations.get(unlocalized);
        }

        return unlocalized;
    }

    @Override
    public void addTranslation(String unlocalized, String localized)
    {
        this.translations.put(unlocalized, localized);
    }

    @Override
    public boolean hasTranslation(String unlocalized)
    {
        return this.translations.containsKey(unlocalized);
    }

    @Override
    public String getLanguage()
    {
        return this.language;
    }

    public static class NullLanguage extends Language {
        public NullLanguage()
        {
            super("nu_LL");
        }

        /**
         * Disabling adding of translations
         *
         * @param unlocalized
         * @param localized
         */
        @Override
        public void addTranslation(String unlocalized, String localized)
        {
        }

        /**
         * Override to return requested language id
         *
         * @param unlocalized
         * @return
         */
        @Override
        public String getTranslation(String unlocalized)
        {
            return unlocalized;
        }

        /**
         * Override to support no strings
         *
         * @param unlocalized
         * @return
         */
        @Override
        public boolean hasTranslation(String unlocalized)
        {
            return false;
        }
    }
}
