package de.yannik_sc.java.language;

import de.yannik_sc.java.language.exceptions.LanguageException;
import de.yannik_sc.java.language.lib.ILanguageIntegrator;
import de.yannik_sc.java.language.lib.ILanguageProvider;
import de.yannik_sc.java.language.lib.ILanguageRegistry;
import de.yannik_sc.java.language.maps.LanguageMap;
import de.yannik_sc.java.language.maps.NamespaceMap;
import de.yannik_sc.java.language.maps.TranslationMap;

/**
 * @author Yannik_Sc
 */
public class LanguageIntegrator implements ILanguageIntegrator
{
    ILanguageRegistry registry;

    public LanguageIntegrator(ILanguageRegistry registry)
    {
        this.registry = registry;
    }

    @Override
    public void integrate(ILanguageProvider provider)
    {
        NamespaceMap namespaces = provider.getTranslations();

        for (String namespace : namespaces.keySet()) {
            LanguageMap languages = namespaces.getLanguage(namespace);

            for (String language : languages.keySet()) {
                TranslationMap translation = languages.getTranslation(language);
                Language lang = new Language(language);

                for (String unlocalized : translation.keySet()) {
                    String localized = translation.getTranslation(unlocalized);
                    lang.addTranslation(unlocalized, localized);
                }

                try {
                    this.registry.registerLanguage(lang, namespace);
                } catch (LanguageException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
