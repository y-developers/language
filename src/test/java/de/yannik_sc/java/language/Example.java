package de.yannik_sc.java.language;

import de.yannik_sc.java.language.exceptions.LanguageException;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * @author Yannik_Sc
 */
public class Example
{
    @Test
    public void main() throws SAXException, ParserConfigurationException, LanguageException, IOException
    {
        // Create a new LanguageRegistry
        LanguageRegistry registry = new LanguageRegistry();

        // Bind a new LanguageIntegrator to the registry
        LanguageIntegrator integrator = new LanguageIntegrator(registry);

        // Create a provide from an xml file
        DomLanguageProvider provider = new DomLanguageProvider(
            new File(ClassLoader.getSystemResource("language.xml").getFile())
        );

        // Integrate the file
        integrator.integrate(provider);

        // Create a request for a translation
        TranslationRequest request = new TranslationRequest("un_IT", "my_app.sub_namespace", "some_string");

        // Get the string
        String result = registry.localize(request);

        // Verify, that the found string, is the wanted string
        Assert.assertEquals("Unit unnitt!", result);
    }
}
