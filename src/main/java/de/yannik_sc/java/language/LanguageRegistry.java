package de.yannik_sc.java.language;

import de.yannik_sc.java.language.exceptions.LanguageException;
import de.yannik_sc.java.language.lib.ILanguage;
import de.yannik_sc.java.language.lib.ILanguageRegistry;
import de.yannik_sc.java.language.lib.ITranslationRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Yannik_Sc
 */
public class LanguageRegistry implements ILanguageRegistry
{
    protected Map<String, Map<String, ILanguage>> languages = new HashMap<>();

    protected static String defaultNamespace = "undefined";

    @Override
    public String localize(ITranslationRequest request)
    {
        return this.findLanguage(request).getTranslation(request.getUnlocalized());
    }

    @Override
    public void registerLanguage(ILanguage language) throws LanguageException
    {
        this.registerLanguage(language, LanguageRegistry.defaultNamespace);
    }

    @Override
    public void registerLanguage(ILanguage language, String namespace) throws LanguageException
    {
        if (!this.languages.containsKey(namespace)) {
            this.languages.put(namespace, new HashMap<>());
        }

        if (this.languages.get(namespace).containsKey(language.getLanguage())) {
            throw new LanguageException(String.format(
                "Language %s already registered in namespace %s.",
                language.getLanguage(),
                namespace
            ));
        }

        this.languages.get(namespace).put(language.getLanguage(), language);
    }

    @Override
    public void unregisterLanguage(String languageName)
    {
        this.unregisterLanguage(languageName, LanguageRegistry.defaultNamespace);
    }

    @Override
    public void unregisterLanguage(String languageName, String namespace)
    {
        if (!this.languages.containsKey(namespace)) {
            return;
        }

        if (!this.languages.get(namespace).containsKey(languageName)) {
            return;
        }

        this.languages.get(namespace).remove(languageName);
    }

    @Override
    public ITranslationRequest createRequest(String languageName, String unlocalizedString)
    {
        return new TranslationRequest(languageName, unlocalizedString);
    }

    @Override
    public boolean hasLanguage(String languageName)
    {
        return this.hasLanguage(languageName, LanguageRegistry.defaultNamespace);
    }

    @Override
    public boolean hasLanguage(String languageName, String namespace)
    {
        if (!this.languages.containsKey(namespace)) {
            return false;
        }

        return this.languages.get(namespace).containsKey(languageName);
    }

    /**
     * @param request a request for a translated string
     * @return a language, that is accepted by the request, or {@link de.yannik_sc.java.language.Language.NullLanguage}
     * in case, no acceptable language is registered
     */
    protected ILanguage findLanguage(ITranslationRequest request)
    {
        ILanguage language;

        if ((language = this.getLanguage(
            request.getNamespace(),
            request.getLanguage(),
            request.getUnlocalized()
        )) != null) {
            return language;
        }

        for (String acceptable : request.getAlternativeLanguages()) {
            if ((language = this.getLanguage(request.getNamespace(), acceptable, request.getUnlocalized())) != null) {
                return language;
            }
        }

        return new Language.NullLanguage();
    }

    protected ILanguage getLanguage(String namespace, String language, String unlocalized)
    {
        if (this.languages.containsKey(namespace) &&
            this.languages.get(namespace).containsKey(language) &&
            this.languages.get(namespace)
                .get(language)
                .hasTranslation(unlocalized)
            ) {
            return this.languages.get(namespace).get(language);
        }

        return null;
    }
}
