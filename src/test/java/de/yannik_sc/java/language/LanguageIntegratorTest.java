package de.yannik_sc.java.language;

import de.yannik_sc.java.language.maps.LanguageMap;
import de.yannik_sc.java.language.maps.NamespaceMap;
import de.yannik_sc.java.language.maps.TranslationMap;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yannik_Sc
 */
public class LanguageIntegratorTest
{
    @Test
    public void integrate()
    {
        LanguageRegistry registry = new LanguageRegistry();
        LanguageIntegrator integrator = new LanguageIntegrator(registry);

        integrator.integrate(() -> {
            TranslationMap translation = new TranslationMap();
            translation.put("some_string", "Hello world");

            LanguageMap language = new LanguageMap();
            language.put("un_IT", translation);

            NamespaceMap namespace = new NamespaceMap();
            namespace.put("some_namespace", language);

            return namespace;
        });

        TranslationRequest request = new TranslationRequest("un_IT", "some_namespace", "some_string");

        String result = registry.localize(request);
        Assert.assertEquals("Hello world", result);
    }
}
