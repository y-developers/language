package de.yannik_sc.java.language.maps;

import java.util.HashMap;

/**
 * @author Yannik_Sc
 */
public class LanguageMap extends HashMap<String, TranslationMap>
{
    public TranslationMap getTranslation(String language)
    {
        return this.get(language);
    }
}
