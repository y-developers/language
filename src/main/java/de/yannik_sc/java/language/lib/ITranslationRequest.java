package de.yannik_sc.java.language.lib;

import java.util.List;

/**
 * @author Yannik_Sc
 */
public interface ITranslationRequest
{
    /**
     * @return the language, in that, the string has to be translated
     */
    String getLanguage();

    /**
     * @return alternatively accepted languages
     */
    List<String> getAlternativeLanguages();

    /**
     * @param language alternatively accepted languages
     */
    void addAlternativeLanguage(String language);

    /**
     * @return the unlocalized string
     */
    String getUnlocalized();

    /**
     * @return the namespace of the string
     */
    String getNamespace();
}
