package de.yannik_sc.java.language;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yannik_Sc
 */
public class LanguageTest
{
    @Test
    public void getLanguage()
    {
        Language lang = new Language("de_DE");
        Language langEn = new Language("en_US");

        Assert.assertEquals("de_DE", lang.getLanguage());
        Assert.assertEquals("en_US", langEn.getLanguage());
    }

    @Test
    public void getTranslation()
    {
        Language lang = new Language("un_IT");
        String translated = "This string has a localization";
        lang.addTranslation("LOCALIZED", translated);

        Assert.assertEquals("UNLOCALIZED", lang.getTranslation("UNLOCALIZED"));
        Assert.assertEquals(translated, lang.getTranslation("LOCALIZED"));
    }

    @Test
    public void addTranslation()
    {
        Language lang = new Language("un_IT");
        String translated1 = "First translation";
        String translated2 = "Second translation";

        lang.addTranslation("LOCALIZED_1", translated1);
        lang.addTranslation("LOCALIZED_2", translated2);

        Assert.assertEquals(translated1, lang.getTranslation("LOCALIZED_1"));
        Assert.assertEquals(translated2, lang.getTranslation("LOCALIZED_2"));
    }

    @Test
    public void hasTranslation()
    {
        Language lang = new Language("un_IT");
        Assert.assertFalse(lang.hasTranslation("UNLOCALIZED"));

        lang.addTranslation("LOCALIZED", "");
        Assert.assertTrue(lang.hasTranslation("LOCALIZED"));
    }
}
