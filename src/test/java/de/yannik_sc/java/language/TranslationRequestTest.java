package de.yannik_sc.java.language;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yannik_Sc
 */
public class TranslationRequestTest
{
    @Test
    public void getLanguage()
    {
        TranslationRequest request = new TranslationRequest("un_IT", "unlocalized");
        Assert.assertEquals("un_IT", request.getLanguage());
    }

    @Test
    public void getUnlocalized()
    {
        TranslationRequest request = new TranslationRequest("un_IT", "unlocalized");
        Assert.assertEquals("unlocalized", request.getUnlocalized());
    }

    @Test
    public void getNamespace()
    {
        TranslationRequest request = new TranslationRequest("un_IT", "noNamespace", "unlocalized");
        Assert.assertEquals("noNamespace", request.getNamespace());

        TranslationRequest request1 = new TranslationRequest("un_IT", "unlocalized");
        Assert.assertEquals("undefined", request1.getNamespace());
    }

    @Test
    public void addGetAlternativeLanguage()
    {
        TranslationRequest request = new TranslationRequest("un_IT", "unlocalized");

        Assert.assertEquals(0, request.getAlternativeLanguages().size());
        request.addAlternativeLanguage("un_IT_new");
        Assert.assertEquals(1, request.getAlternativeLanguages().size());
    }
}
