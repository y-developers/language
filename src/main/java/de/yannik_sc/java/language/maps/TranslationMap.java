package de.yannik_sc.java.language.maps;

import java.util.HashMap;

/**
 * @author Yannik_Sc
 */
public class TranslationMap extends HashMap<String, String>
{
    public String getTranslation(String unlocalizedName)
    {
        return this.get(unlocalizedName);
    }
}
