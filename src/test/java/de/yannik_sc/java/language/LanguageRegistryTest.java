package de.yannik_sc.java.language;

import de.yannik_sc.java.language.exceptions.LanguageException;
import de.yannik_sc.java.language.lib.ILanguage;
import de.yannik_sc.java.language.lib.ITranslationRequest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

/**
 * @author Yannik_Sc
 */
public class LanguageRegistryTest
{
    @Test
    public void localizeNull()
    {
        LanguageRegistry registry = new LanguageRegistry();
        ITranslationRequest request = this.getRequest("un_IT", "UNLOCALIZED");

        String localized = registry.localize(request);
        Assert.assertEquals("UNLOCALIZED", localized);

        Mockito.verify(request, Mockito.atLeastOnce()).getLanguage();
        Mockito.verify(request, Mockito.times(1)).getAlternativeLanguages();
        Mockito.verify(request, Mockito.atLeast(1)).getUnlocalized();
    }

    @Test
    public void localizeNoResult() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();
        registry.registerLanguage(new Language.NullLanguage());
        ITranslationRequest request = this.getRequest("un_IT", "UNLOCALIZED");

        String localized = registry.localize(request);
        Assert.assertEquals("UNLOCALIZED", localized);

        Mockito.verify(request, Mockito.atLeastOnce()).getLanguage();
        Mockito.verify(request, Mockito.times(1)).getAlternativeLanguages();
        Mockito.verify(request, Mockito.atLeast(1)).getUnlocalized();
    }

    @Test
    public void localizeNoResultMultipleLanguages() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();
        registry.registerLanguage(new Language.NullLanguage());
        registry.registerLanguage(this.getLanguage("nu_LL_new", "", ""));
        ITranslationRequest request = this.getRequest("un_IT", "UNLOCALIZED", "un_IT_new");

        String localized = registry.localize(request);
        Assert.assertEquals("UNLOCALIZED", localized);

        Mockito.verify(request, Mockito.atLeast(1)).getLanguage();
        Mockito.verify(request, Mockito.times(1)).getAlternativeLanguages();
        Mockito.verify(request, Mockito.atLeast(1)).getUnlocalized();
    }

    @Test
    public void localizeMatch() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();
        ILanguage language = this.getLanguage("un_IT", "LOCALIZED", "Translated!");
        registry.registerLanguage(language);
        ITranslationRequest request = this.getRequest("un_IT", "LOCALIZED");

        String localized = registry.localize(request);
        Assert.assertEquals("Translated!", localized);
        Mockito.verify(language, Mockito.times(1)).getTranslation("LOCALIZED");
        Mockito.verify(language, Mockito.times(1)).hasTranslation("LOCALIZED");
    }

    @Test
    public void localizeMatchAlt() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();
        registry.registerLanguage(new Language.NullLanguage());
        registry.registerLanguage(this.getLanguage("un_IT_new", "LOCALIZED", "Translated!"));
        ITranslationRequest request = this.getRequest("un_IT", "LOCALIZED", "un_IT_new");

        String localized = registry.localize(request);
        Assert.assertEquals("Translated!", localized);
        Mockito.verify(request, Mockito.times(1)).getAlternativeLanguages();
    }

    @Test
    public void registerLanguageHasLanguage() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();
        ILanguage language = this.getLanguage("un_IT", "LOCALIZED", "Translated!");

        Assert.assertFalse(registry.hasLanguage("un_IT"));
        registry.registerLanguage(language);

        Mockito.verify(language, Mockito.times(2)).getLanguage();
        Assert.assertTrue(registry.hasLanguage("un_IT"));
    }

    @Test
    public void unregisterLanguageHasLanguage() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();
        ILanguage language = this.getLanguage("un_IT", "LOCALIZED", "Translated!");

        Assert.assertFalse(registry.hasLanguage("un_IT"));
        registry.registerLanguage(language);

        Mockito.verify(language, Mockito.times(2)).getLanguage();
        Assert.assertTrue(registry.hasLanguage("un_IT"));

        registry.unregisterLanguage("un_IT");
        Assert.assertFalse(registry.hasLanguage("un_IT"));
    }

    @Test
    public void registerAlreadyRegisteredLanguage() throws LanguageException
    {
        LanguageRegistry registry = new LanguageRegistry();

        registry.registerLanguage(new Language.NullLanguage());
        try {
            registry.registerLanguage(new Language.NullLanguage());

            Assert.fail("Overriding already defined language has to fail!");
        } catch (LanguageException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void createRequest()
    {
        LanguageRegistry registry = new LanguageRegistry();
        ITranslationRequest request = registry.createRequest("un_IT", "UNLOCALIZED");

        Assert.assertNotNull(request);
        Assert.assertEquals("un_IT", request.getLanguage());
        Assert.assertEquals("UNLOCALIZED", request.getUnlocalized());
    }

    protected ITranslationRequest getRequest(String lang, String unlocalized, String... altLangs)
    {
        ITranslationRequest request = Mockito.mock(ITranslationRequest.class);
        Mockito.when(request.getLanguage()).thenReturn(lang);
        Mockito.when(request.getUnlocalized()).thenReturn(unlocalized);
        Mockito.when(request.getNamespace()).thenReturn(LanguageRegistry.defaultNamespace);
        Mockito.when(request.getAlternativeLanguages()).thenReturn(Arrays.asList(altLangs));

        return request;
    }

    protected ILanguage getLanguage(String lang, String unlocalized, String localized)
    {
        ILanguage language = Mockito.mock(ILanguage.class);

        Mockito.when(language.getLanguage()).thenReturn(lang);
        Mockito.when(language.getTranslation(unlocalized)).thenReturn(localized);
        Mockito.when(language.hasTranslation(unlocalized)).thenReturn(true);

        return language;
    }
}
