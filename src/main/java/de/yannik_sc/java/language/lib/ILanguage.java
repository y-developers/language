package de.yannik_sc.java.language.lib;

/**
 * @author Yannik_Sc
 */
public interface ILanguage
{
    String getTranslation(String unlocalized);

    void addTranslation(String unlocalized, String localized);

    boolean hasTranslation(String unlocalized);

    String getLanguage();
}
