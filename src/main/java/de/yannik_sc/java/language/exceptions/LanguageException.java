package de.yannik_sc.java.language.exceptions;

/**
 * @author Yannik_Sc
 */
public class LanguageException extends Exception
{
    public LanguageException(String s)
    {
        super(s);
    }
}
