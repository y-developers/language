[![Release](https://jitpack.io/v/com.gitlab.y-developers/language.svg)]
(https://jitpack.io/#com.gitlab.y-developers/language)
# Java "Language"

## Using this project

You can use this project via gradle, maven, sbt and leiningen, by requiring it as dependency from jitpack.
[More info](https://jitpack.io/#com.gitlab.y-developers/language)

## About this Project
This project focuses on translation. It should help you, to manage an application which supports multiple 
languages. Languages can be defined in an xml/ini/yml/json file or even in a database.

### Get a String
To get a string, you have to create a `TranslationRequest` and give it to the `LanguageRegistry`

### Import languages
To define your languages this package, provides an XML integrator, which can read an XML file and extract its 
data into the LanguageRegistry.

This is done by passing a `DomLanguageProvider` into a `LanguageIntegrator`, which is bind to a `LanguageRegistry`

#### Example
You can find an example, in the test folder, where an XML file gets imported into a `LanguageRegistry`. After its 
import the a string is requested and is checked, if its the correct one.

#### Custom provider
You can create easily your own provider, by implementing the `ILanguageProvider` interface. This package only
helps you (as already said) by importing XML files so you probably want to create your own provider for a
database or some other file types.

#### XML Import
To help you with the XML import, you can find a XML schema in the "schema" folder in this project