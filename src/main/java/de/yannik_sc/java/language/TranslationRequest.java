package de.yannik_sc.java.language;

import de.yannik_sc.java.language.lib.ITranslationRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yannik_Sc
 */
public class TranslationRequest implements ITranslationRequest
{
    protected String language;
    protected List<String> alternativeLanguages = new ArrayList<>();
    protected String unlocalized;
    protected String namespace;

    public TranslationRequest(String language, String unlocalized)
    {
        this(language, LanguageRegistry.defaultNamespace, unlocalized);
    }

    public TranslationRequest(String language, String namespace, String unlocalized)
    {
        this.language = language;
        this.unlocalized = unlocalized;
        this.namespace = namespace;
    }

    @Override
    public String getLanguage()
    {
        return this.language;
    }

    @Override
    public List<String> getAlternativeLanguages()
    {
        return this.alternativeLanguages;
    }

    @Override
    public void addAlternativeLanguage(String language)
    {
        this.alternativeLanguages.add(language);
    }

    @Override
    public String getUnlocalized()
    {
        return this.unlocalized;
    }

    @Override
    public String getNamespace()
    {
        return this.namespace;
    }
}
