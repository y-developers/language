package de.yannik_sc.java.language;

import de.yannik_sc.java.language.exceptions.LanguageException;
import de.yannik_sc.java.language.maps.NamespaceMap;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * @author Yannik_Sc
 */
public class DomLanguageProviderTest
{
    @Test
    public void getLanguages() throws IOException, SAXException, ParserConfigurationException, LanguageException
    {
        DomLanguageProvider provider = this.getProvider();
        NamespaceMap translations = provider.getTranslations();

        translations.get("my_app.sub_namespace").get("un_IT").get("some_string");

        Assert.assertTrue(translations.containsKey("my_app.sub_namespace"));
        Assert.assertTrue(translations.get("my_app.sub_namespace").containsKey("un_IT"));
        Assert.assertTrue(translations.get("my_app.sub_namespace").containsKey("en_US"));
        Assert.assertFalse(translations.get("my_app.sub_namespace").containsKey("de_DE"));
        Assert.assertTrue(translations.get("my_app.sub_namespace").get("en_US").containsKey("some_string"));
        Assert.assertEquals("Hello world!", translations.get("my_app.sub_namespace").get("en_US").get("some_string"));
    }

    protected DomLanguageProvider getProvider() throws ParserConfigurationException, SAXException, IOException, LanguageException
    {
        DomLanguageProvider provider = new DomLanguageProvider(
            new File(
                ClassLoader.getSystemResource("language.xml").getFile()
            )
        );

        return provider;
    }
}
