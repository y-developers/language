package de.yannik_sc.java.language;

import de.yannik_sc.java.language.exceptions.LanguageException;
import de.yannik_sc.java.language.lib.ILanguageProvider;
import de.yannik_sc.java.language.maps.LanguageMap;
import de.yannik_sc.java.language.maps.NamespaceMap;
import de.yannik_sc.java.language.maps.TranslationMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * @author Yannik_Sc
 */
public class DomLanguageProvider implements ILanguageProvider
{
    File languageFile;
    NamespaceMap translations = new NamespaceMap();

    public DomLanguageProvider(File languageFile) throws IOException, SAXException, ParserConfigurationException, LanguageException
    {
        this.languageFile = languageFile;
        this.loadFile();
    }

    @Override
    public NamespaceMap getTranslations()
    {
        return this.translations;
    }

    protected void loadFile() throws ParserConfigurationException, IOException, SAXException, LanguageException
    {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        Document languageDocument = builder.parse(this.languageFile);

        this.verifyDocument(languageDocument);
    }

    protected void verifyDocument(Document languageDocument) throws LanguageException
    {
        Element language = languageDocument.getDocumentElement();

        if (!language.getTagName().equals("languages")) {
            throw new LanguageException("Node 'language' is expected as XML document node");
        }

        this.integrateDocument(language.getChildNodes());
    }

    protected void integrateDocument(NodeList childNodes)
    {
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node child = childNodes.item(i);

            if (child.getNodeName().startsWith("#")) {
                continue;
            }

            String namespace = child.getAttributes().getNamedItem("name").getNodeValue();

            NodeList translations = child.getChildNodes();

            for (int j = 0; j < translations.getLength(); j++) {
                Node translationsNode = translations.item(j);

                if (translationsNode.getNodeName().startsWith("#")) {
                    continue;
                }

                String language = translationsNode.getAttributes().getNamedItem("language").getNodeValue();

                NodeList translation = translationsNode.getChildNodes();


                for (int k = 0; k < translations.getLength(); k++) {
                    Node translationNode = translation.item(k);

                    if (translationNode.getNodeName().startsWith("#")) {
                        continue;
                    }

                    String name = translationNode.getAttributes().getNamedItem("name").getNodeValue();
                    String value = translationNode.getTextContent();

                    this.setTranslation(namespace, language, name, value);
                }
            }
        }
    }

    protected void setTranslation(String namespace, String language, String unlocalized, String localized)
    {
        if (!this.translations.containsKey(namespace)) {
            this.translations.put(namespace, new LanguageMap());
        }

        if (!this.translations.get(namespace).containsKey(language)) {
            this.translations.get(namespace).put(language, new TranslationMap());
        }

        if (!this.translations.get(namespace).get(language).containsKey(unlocalized)) {
            this.translations.get(namespace).get(language).put(unlocalized, localized);
        }
    }
}
