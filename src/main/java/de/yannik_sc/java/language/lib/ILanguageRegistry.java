package de.yannik_sc.java.language.lib;

import de.yannik_sc.java.language.exceptions.LanguageException;

/**
 * @author Yannik_Sc
 */
public interface ILanguageRegistry
{
    String localize(ITranslationRequest request);

    void registerLanguage(ILanguage language) throws LanguageException;

    void registerLanguage(ILanguage language, String namespace) throws LanguageException;

    void unregisterLanguage(String languageName);

    void unregisterLanguage(String languageName, String namespace);

    ITranslationRequest createRequest(String languageName, String unlocalizedString);

    boolean hasLanguage(String languageName);

    boolean hasLanguage(String languageName, String namespace);
}
