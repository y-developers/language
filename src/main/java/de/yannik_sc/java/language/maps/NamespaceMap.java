package de.yannik_sc.java.language.maps;

import java.util.HashMap;

/**
 * @author Yannik_Sc
 */
public class NamespaceMap extends HashMap<String, LanguageMap>
{
    public LanguageMap getLanguage(String namespace)
    {
        return this.get(namespace);
    }
}
